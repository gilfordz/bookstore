package com.example.bookstoreadmin.ui.main.fragment.profile

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bookstore.admin.R
import com.example.bookstoreadmin.signin.SignInActivity
import com.example.bookstoreadmin.ui.book.edit.EditBookActivity
import com.example.bookstoreadmin.ui.main.MainActivity
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.example.bookstoreadmin.ui.profile.AboutApplicationDialog
import com.example.bookstoreadmin.ui.settings.settings
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class ProfileFragment : Fragment() {
    private  val mainViewModelAdm : MainViewModelAdm by sharedViewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_logout.setOnClickListener {
           createCustomTwoButtonDialog()
        }
        row_menu_about.setOnClickListener {
            AboutApplicationDialog().show(requireActivity().supportFragmentManager,
                AboutApplicationDialog.TAG)
        }
        row_menu_settings.setOnClickListener {
            startActivity(Intent(requireContext(), settings::class.java))
        }
    }
    fun createCustomTwoButtonDialog() {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle("confirmation !!")
        builder.setMessage("Are you want logout?")
        builder.setPositiveButton("YES") { dialog, which ->
            mainViewModelAdm.logout(requireActivity())
            val intent = Intent(requireContext(), SignInActivity::class.java)
            startActivity(intent)
        }
        builder.setNegativeButton("No") { dialog, which ->
            Toast.makeText(requireActivity(), "You are not agree.", Toast.LENGTH_SHORT).show()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
    }


