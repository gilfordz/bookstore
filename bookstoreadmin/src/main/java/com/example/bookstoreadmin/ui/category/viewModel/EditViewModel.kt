package com.example.bookstoreadmin.ui.category.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bookstore.constant.RetrofitStatus
import com.example.bookstoreadmin.model.formatted.category.CategoryResponse
import com.example.bookstoreadmin.model.request.category.CategoryBookRequest
import com.example.bookstoreadmin.repository.CateRepository
import com.example.utils.Retrofit.printRetrofitError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException

class EditViewModel(
    application: Application,
    private val categoryRepository: CateRepository
):AndroidViewModel(application) {
    private val _cateResponse = MutableLiveData<CategoryResponse>()
    val categoryResponse:LiveData<CategoryResponse> = _cateResponse

    fun getCategory() = viewModelScope.launch (Dispatchers.IO){
        try {
            val result = categoryRepository.getCategories().sortedBy { it.id }
            if(result.isNotEmpty()) _cateResponse.postValue(CategoryResponse(RetrofitStatus.SUCCESS, result))
            else{
                _cateResponse.postValue(CategoryResponse(RetrofitStatus.EMPTY))
            }
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _cateResponse.postValue(CategoryResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _cateResponse.postValue(CategoryResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
    private val _addBookCategories = MutableLiveData<CategoryResponse>()
    val addBookCategories:LiveData<CategoryResponse> = _addBookCategories

    fun addBookCategories(id:Int,code:String,name:String) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val categoryBookRequest = CategoryBookRequest(id,code,name)
            val result = categoryRepository.addBookToCategory(categoryBookRequest)
            if(result.isSuccessful){
                _cateResponse.postValue(CategoryResponse(RetrofitStatus.SUCCESS))
            }else{
                _cateResponse.postValue(CategoryResponse(RetrofitStatus.FAILURE))
            }
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _cateResponse.postValue(CategoryResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _cateResponse.postValue(CategoryResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
    private val _updateBookCategoryResponse = MutableLiveData<CategoryResponse>()
    val updateBookCategoryResponse: LiveData<CategoryResponse> = _updateBookCategoryResponse

    fun updateBookCategories(id:Int,code: String,name: String) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val categoryBookRequest = CategoryBookRequest(id,code,name)
            val result = categoryRepository.updateBookCategories(categoryBookRequest)
            if(result.isSuccessful){
                _updateBookCategoryResponse.postValue(CategoryResponse(RetrofitStatus.SUCCESS))
            }else{
                _updateBookCategoryResponse.postValue(CategoryResponse(RetrofitStatus.FAILURE))
            }
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _updateBookCategoryResponse.postValue(CategoryResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _updateBookCategoryResponse.postValue(CategoryResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
            }
        }
    private val _removeCategoryResponse = MutableLiveData<CategoryResponse>()
    val removeCategoryResponse:LiveData<CategoryResponse> = _removeCategoryResponse

    fun removeFromCategory(id:Int) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val result = categoryRepository.removeFromCategory(id)
            if (result.isSuccessful){
                _removeCategoryResponse.postValue(CategoryResponse(RetrofitStatus.SUCCESS))
            }else{
                _removeCategoryResponse.postValue(CategoryResponse(RetrofitStatus.FAILURE))
            }
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
            _removeCategoryResponse.postValue(CategoryResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _removeCategoryResponse.postValue(CategoryResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
    }