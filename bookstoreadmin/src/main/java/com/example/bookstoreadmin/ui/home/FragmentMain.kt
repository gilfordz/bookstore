package com.example.bookstoreadmin.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bookstore.admin.R
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.response.book.Book
import com.bookstore.model.response.book.BookCategory
import com.example.bookstoreadmin.ui.adapter.category.CategoryItemListener
import com.example.bookstoreadmin.ui.book.edit.EditBookActivity
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.example.ui.book.adapter.BookItemListener
import com.example.utils.ViewHelper.hide
import com.example.utils.ViewHelper.show
import kotlinx.android.synthetic.main.fragment_book.*
import kotlinx.android.synthetic.main.fragment_book.placeholder_empty
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class FragmentMain : Fragment(), BookItemListener,CategoryItemListener {
    private val mainViewModel: MainViewModelAdm by sharedViewModel()
    private val overViewModel: OverViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_home,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        overViewModel.bookresponse.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                RetrofitStatus.SUCCESS -> it.list?.let { list ->
                    count_book_home.text = list.size.toString()
                }
                RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(requireActivity())
                else -> {
                    recyclerview.hide()
                    placeholder_empty.show()
                }
            }
        })
        overViewModel.getCountBook()

        overViewModel.categoryResponse.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                RetrofitStatus.SUCCESS -> it.list?.let { list ->
                    count_categories.text = list.size.toString()
                }
                RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(requireActivity())
                else -> {
                    recyclerview.hide()
                    placeholder_empty.show()
                }
            }
        })
        overViewModel.getCountCategory()

        button_available.setOnClickListener{
            val intent = Intent(requireContext(), EditBookActivity::class.java)
            startActivity(intent)
        }
        button_category.setOnClickListener{
        }
        button_purchases.setOnClickListener{
        }
    }

    override fun onItemClick(book: Book) {
        TODO("Not yet implemented")
    }

    override fun onItemClick(categories: BookCategory) {
        TODO("Not yet implemented")
    }

}