package com.example.ui.book.adapter
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bookstore.admin.R
import com.bookstore.constant.BookStatus
import com.bookstore.model.response.book.Book
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.bookstoreadmin.ui.book.addOn.BookFilterable
import kotlinx.android.synthetic.main.item_list_book.view.*

class BookAdapter(
    private val bookItemListener:BookItemListener):RecyclerView.Adapter<BookAdapter.ViewHolder>(),
    BookFilterable {
    private val originalBooks = mutableListOf<Book>()
    private var books = listOf<Book>()


    fun setData(book:List<Book>){
        this.originalBooks.clear()
        books = book
        this.originalBooks.addAll(book.filter { it.bookStatus== BookStatus.FOR_SELL.toString() })
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
    ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list_book, parent, false ))

    @SuppressLint("DefaultLint")
    override fun performFilterByName(bookName: String?) {
        books = originalBooks
        if (!bookName.isNullOrEmpty()) books = originalBooks.filter {
            it.title.trim().toLowerCase().contains(bookName.trim().toLowerCase())
        }
        books.sortedBy { it.id }
//        bookItemListener.onItemSearch(bookz.isEmpty())
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return books.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(books[position])

    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        @SuppressLint("DefaultLocale")
        fun bind(book:Book){
            Glide.with(itemView.context)
                .load(book.imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .transition(DrawableTransitionOptions.withCrossFade())
                .centerCrop()
                .placeholder(R.color.colorShimmer)
                .error(R.color.colorShimmer)
                .into(itemView.image_cover)
                itemView.text_name.text = book.title.trim().capitalize()
                itemView.text_category.text= book.bookCategory.name.trim().capitalize()
                itemView.card.setOnClickListener{
                    bookItemListener.onItemClick(book)
                }
        }

    }
}