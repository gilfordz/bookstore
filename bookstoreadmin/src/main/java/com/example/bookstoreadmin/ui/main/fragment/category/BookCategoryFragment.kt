package com.example.bookstoreadmin.ui.main.fragment.category

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bookstore.admin.R
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.response.book.BookCategory
import com.example.bookstoreadmin.ui.adapter.category.CategoryAdapter
import com.example.bookstoreadmin.ui.adapter.category.CategoryItemListener
import com.example.bookstoreadmin.ui.category.add.AddCategories
import com.example.bookstoreadmin.ui.category.edit.EditCategories
import com.example.bookstoreadmin.ui.category.viewModel.EditViewModel
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.example.utils.ViewHelper.hide
import com.example.utils.ViewHelper.hideKeyboard
import com.example.utils.ViewHelper.show
import kotlinx.android.synthetic.main.fragment_book_category.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class BookCategoryFragment : Fragment(),
    CategoryItemListener {
    private val mainViewModelCategory : EditViewModel by viewModel()
    private val categoryAdapter by lazy {
        CategoryAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_book_category, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainViewModelCategory.categoryResponse.observe(viewLifecycleOwner, Observer {
            when(it.status){
                RetrofitStatus.SUCCESS -> it.list?.let { list ->
                    placeholder_empty.hide()
                    loading.hide()
                    recyclerview_fbc.show()
                    categoryAdapter.setData(list)
                    text_category_count.text = list.size.toString()
                }
                else -> {
                    recyclerview_fbc.hide()
                    placeholder_empty.show()
                }
            }
        })
        button_adds.setOnClickListener {
            AddCategories().show(requireActivity().supportFragmentManager,AddCategories.DATA)
        }
        button_search.setOnClickListener {
            input_search.show()
            text_title_bc.hide()
        }
        input_search.setOnEditorActionListener{ _,actionid,_->
            if (actionid == EditorInfo.IME_ACTION_SEARCH){
                categoryAdapter.performFilterByName(input_search.text.toString())
                input_search.hideKeyboard()
            }
            true
        }
        recyclerview_fbc.apply {
            adapter = categoryAdapter
            layoutManager = StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL)
            setHasFixedSize(true)
        }
        swipe_refresh_layout.setOnRefreshListener {
           mainViewModelCategory.getCategory()
            swipe_refresh_layout.isRefreshing = false
        }
            }

    override fun onResume() {
        super.onResume()
        mainViewModelCategory.getCategory()
    }

    override fun onItemClick(categories: BookCategory) {
        val intent = Intent(requireActivity(), EditCategories::class.java)
        EditCategories(intent.putExtra(EditCategories.DATA,categories)).show(requireActivity().supportFragmentManager,EditCategories.DATA)
    }
}