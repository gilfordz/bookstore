package com.example.bookstoreadmin.ui.category.add

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bookstore.admin.R
import com.bookstore.constant.RetrofitStatus
import com.example.bookstoreadmin.signin.SignInActivity
import com.example.bookstoreadmin.ui.category.viewModel.EditViewModel
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.example.bookstoreadmin.ui.main.fragment.category.BookCategoryFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.dialog_add_book_category.*
import kotlinx.android.synthetic.main.dialog_add_book_category.button_save
import kotlinx.android.synthetic.main.dialog_add_book_category.parent_layout
import kotlinx.android.synthetic.main.fragment_book_category.*
import org.koin.android.viewmodel.ext.android.viewModel

class AddCategories : BottomSheetDialogFragment(){
    companion object{
        const val DATA = "book_add_activity_data"
    }
    private val editViewModel : EditViewModel by viewModel()
    private val mainViewModel : MainViewModelAdm by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_add_book_category,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_save.setOnClickListener {
            editViewModel.addBookCategories(0,text_code_add.text.toString(),text_name_add.text.toString())
            dialog?.dismiss()
            dialog?.cancel()
        }
        button_cancel.setOnClickListener {
            dialog?.dismiss()
            dialog?.cancel()
        }
    editViewModel.addBookCategories.observe(this, Observer {
        when(it.status){
            RetrofitStatus.SUCCESS ->{ showSnackbar("Successfully") }
            RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(requireActivity())
            else-> showSnackbar("Error occured when add category")
            }
    })
    }
    private fun showSnackbar(msg:String) = Snackbar.make(parent_layout,msg, Snackbar.LENGTH_SHORT).show()
}