package com.example.bookstoreadmin.ui.adapter.category
import com.bookstore.model.response.book.BookCategory

interface CategoryItemListener {
    fun onItemClick(categories: BookCategory)
}