package com.example.bookstoreadmin.ui.book.add

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import com.bookstore.admin.R
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.response.book.BookCategory
import com.example.bookstoreadmin.ui.adapter.category.CategoryAdapter
import com.example.bookstoreadmin.ui.adapter.category.CategoryItemListener
import com.example.bookstoreadmin.ui.book.viewModel.DetailBookVMAdmin
import com.example.bookstoreadmin.ui.category.viewModel.EditViewModel
import com.example.bookstoreadmin.ui.main.MainActivity
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.example.utils.ViewHelper.hide
import com.example.utils.ViewHelper.show
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_add_book.*
import kotlinx.android.synthetic.main.activity_add_book.button_back
import kotlinx.android.synthetic.main.activity_add_book.button_save
import kotlinx.android.synthetic.main.activity_add_book.input_book_status
import kotlinx.android.synthetic.main.dialog_add_book_category.parent_layout
import kotlinx.android.synthetic.main.fragment_book_category.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.time.LocalDateTime

class AddBookActivity : AppCompatActivity(),CategoryItemListener {
    companion object{
        const val DATA = "book_add_data"
    }
    private val mainViewModelCategory : EditViewModel by viewModel()
    private val categoryAdapter by lazy {
        CategoryAdapter(this)
    }
    private val detailBookVMAdmin : DetailBookVMAdmin by viewModel()
    private val mainViewModelAdm : MainViewModelAdm by viewModel()
    private var bookKategori = listOf<BookCategory>()
    private var bookCategoryString = arrayListOf<String>()
    private var id:Int = 0
    @RequiresApi(Build.VERSION_CODES.O)
    private  val current = LocalDateTime.now()
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_book)
        // access the items of the list
        val languages = resources.getStringArray(R.array.Languages)

        // access the spinner
        val spinner = findViewById<Spinner>(R.id.input_text_book_status_new)
        if (spinner != null) {
            val adapter = ArrayAdapter(this,
                android.R.layout.simple_spinner_item, languages)
            spinner.adapter = adapter

            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    val text: String = parent?.getItemAtPosition(position).toString()
                    input_book_status.text = text
                    Toast.makeText(this@AddBookActivity,
                        getString(R.string.selected_item) + " " +
                                "" + languages[position], Toast.LENGTH_SHORT).show()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
        val spinner_1 = findViewById<Spinner>(R.id.input_text_book_category_new)

        fun setBookCategory(){
            if (spinner_1 != null) {
                val adapter = ArrayAdapter(this,
                    android.R.layout.simple_list_item_1,  bookCategoryString)
                spinner_1.adapter = adapter

                spinner_1.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>,
                                                view: View, position: Int, id: Long) {
                        val text: String = parent?.getItemAtPosition(position).toString()
                        textViewCategory.text = text
                        Toast.makeText(this@AddBookActivity,
                            getString(R.string.selected_item) + " " +
                                    "" +  bookCategoryString[position], Toast.LENGTH_SHORT).show()
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                        // write code to perform some action
                    }
                }
            }
        }
        mainViewModelCategory.categoryResponse.observe(this, Observer {
            when(it.status){
                RetrofitStatus.SUCCESS -> it.list?.let { list ->
                    categoryAdapter.setData(list)
                    bookKategori = list
                    bookCategoryString.clear()
                    bookKategori.map {
                        bookCategoryString.add(it.name)
                    }
                    setBookCategory()
                }
                RetrofitStatus.UNAUTHORIZED -> mainViewModelAdm.logout(this)
                else -> {
                    recyclerview_fbc.hide()
                    placeholder_empty.show()
                }
            }
        })
                button_back.setOnClickListener {
                    super.onBackPressed()
                }
                button_save.setOnClickListener {
                   filterKode(textViewCategory.text.toString())
                    detailBookVMAdmin.addBook(
                        text_authors_new.text.toString(),
                        id,  input_book_status.text.toString(),
                        0,
                        "A",
                        text_price_new.text.toString().toInt(),
                        current.toString(),
                        text_synopsis_new.text.toString(),
                        text_title_new.text.toString()
                    )
                }
                detailBookVMAdmin.addBookResponse.observe(this, Observer {
                    when (it.status) {
                        RetrofitStatus.SUCCESS -> {
                            super.onBackPressed()
                            startActivity(Intent(this, MainActivity::class.java))
                        }
                        RetrofitStatus.UNAUTHORIZED -> mainViewModelAdm.logout(this)
                        else -> showSnackbar("Error occured when add category")
                    }
                })
            }
        private fun showSnackbar(msg: String) =
            Snackbar.make(parent_layout, msg, Snackbar.LENGTH_SHORT).show()

        private fun filterKode(categoryName: String?){
           for (i in 0..bookKategori.size-1){
               if(bookKategori[i].name==categoryName){
                   id = bookKategori[i].id
               }
               }
           }
        override fun onItemClick(categories: BookCategory) {
            TODO("Not yet implemented")
        }
    override fun onResume() {
        super.onResume()
        mainViewModelCategory.getCategory()
    }
    }