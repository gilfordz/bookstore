package com.example.bookstoreadmin.ui.book.viewModel

import android.app.Application
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bookstore.constant.BookStatus
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.formatted.book.BookResponse
import com.example.bookstoreadmin.model.request.book.BookRequest
import com.example.bookstoreadmin.model.request.book.GambarRequest
import com.example.bookstoreadmin.repository.BookRespository
import com.example.utils.Retrofit.printRetrofitError
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.File
import java.time.format.DateTimeFormatter

class DetailBookVMAdmin(
    application: Application,
    private val bookRespository: BookRespository
): AndroidViewModel(application) {

    private val _updateBookResponse = MutableLiveData<BookResponse>()
    val updateBookResponse: LiveData<BookResponse> = _updateBookResponse

    @RequiresApi(Build.VERSION_CODES.O)
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")

    @RequiresApi(Build.VERSION_CODES.O)
    fun updateBook(
        authorName: String,
        bookCategoryId: Int,
        bookStatus: String,
        id: Int,
        isbn: String,
        price: Int,
        publicationDate: String,
        synopsis: String,
        title: String
    ) = viewModelScope.launch(Dispatchers.IO) {
        try {

            val bookRequest = BookRequest(
                authorName,
                bookCategoryId,
                bookStatus,
                id,
                isbn,
                price,
                publicationDate,
                synopsis,
                title
            )
            Log.i("TEST", "updateBook: ${Gson().toJson(bookRequest)}")
            val result = bookRespository.updateBook(bookRequest)
            if (result.isSuccessful) {
                _updateBookResponse.postValue(BookResponse(RetrofitStatus.SUCCESS))
            } else {
                _updateBookResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            }
        } catch (throwable: Throwable) {
            if (throwable is HttpException && throwable.code() == 401)
                _updateBookResponse.postValue(BookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _updateBookResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }

    private val _addBookResponse = MutableLiveData<BookResponse>()
    val addBookResponse: LiveData<BookResponse> = _addBookResponse

    @RequiresApi(Build.VERSION_CODES.O)
    fun addBook(
        authorName: String,
        bookCategoryId: Int,
        bookStatus: String,
        id: Int,
        isbn: String,
        price: Int,
        publicationDate: String,
        synopsis: String,
        title: String
    ) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val bookRequest = BookRequest(
                authorName,
                bookCategoryId,
                bookStatus,
                id,
                isbn,
                price,
                publicationDate,
                synopsis,
                title
            )
            val result = bookRespository.addBook(bookRequest)
            if (result.isSuccessful) {
                _addBookResponse.postValue(BookResponse(RetrofitStatus.SUCCESS))
            } else {
                _addBookResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            }
        } catch (throwable: Throwable) {
            if (throwable is HttpException && throwable.code() == 401)
                _addBookResponse.postValue(BookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _addBookResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }

    private val _addBGambarResponse = MutableLiveData<BookResponse>()
    val addGambarResponse: LiveData<BookResponse> = _addBookResponse
    fun uploadImage(file: File, id: Int) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val bookRequestImg = GambarRequest(file, id)
            Log.i("TEST", "updateGambarBook: ${Gson().toJson(bookRequestImg)}")
            val result = bookRespository.uploadImage(file, id)
            if (result.isSuccessful) {
                _addBGambarResponse.postValue(BookResponse(RetrofitStatus.SUCCESS))
            } else {
                _addBGambarResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            }
        } catch (throwable: Throwable) {
            if (throwable is HttpException && throwable.code() == 401)
                _addBGambarResponse.postValue(BookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _addBGambarResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }

    private val _removeBookResponse = MutableLiveData<BookResponse>()
    val removeBookResponse: LiveData<BookResponse> = _removeBookResponse

    fun removeBook(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        try {
            // val category = CategoryBookRequest(id,code,name)
            val result = bookRespository.removeBook(id)
            if (result.isSuccessful) {
                _removeBookResponse.postValue(BookResponse(RetrofitStatus.SUCCESS))
            } else {
                _removeBookResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            }
        } catch (throwable: Throwable) {
            if (throwable is HttpException && throwable.code() == 401)
                _removeBookResponse.postValue(BookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _removeBookResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }

    private val _bookResponse = MutableLiveData<BookResponse>()
    val bookresponse: LiveData<BookResponse> = _bookResponse

    fun getBook() = viewModelScope.launch(Dispatchers.IO) {
        try {
            val result = bookRespository.getBook().sortedBy { it.id }
                .filter { it.bookStatus == BookStatus.FOR_SELL.toString() }
            if (result.isNotEmpty()) _bookResponse.postValue(
                BookResponse(
                    RetrofitStatus.SUCCESS,
                    result
                )
            )
            else {
                _bookResponse.postValue(BookResponse(RetrofitStatus.EMPTY))
                Log.e(this::class.java.simpleName, result.toString())
            }
        } catch (throwable: Throwable) {
            if (throwable is HttpException && throwable.code() == 401)
                _bookResponse.postValue(BookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _bookResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
}