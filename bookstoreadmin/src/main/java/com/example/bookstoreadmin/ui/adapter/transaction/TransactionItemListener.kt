package com.example.bookstoreadmin.ui.adapter.transaction

import com.bookstore.model.response.transaction.Transaction

interface TransactionItemListener {
    fun onItemClick(transaction: Transaction)
}