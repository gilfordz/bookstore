package com.example.bookstoreadmin.ui.main.fragment.purchase

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bookstore.admin.R
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.response.transaction.Transaction
import com.example.bookstoreadmin.ui.adapter.transaction.TransactionAdapter
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.example.bookstoreadmin.ui.adapter.transaction.TransactionItemListener
import com.example.utils.ViewHelper.hide
import com.example.utils.ViewHelper.show
import kotlinx.android.synthetic.main.fragment_book.placeholder_empty
import kotlinx.android.synthetic.main.fragment_book.swipe_refresh_layout
import kotlinx.android.synthetic.main.fragment_purchase.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class PurchaseFragment : Fragment(), TransactionItemListener {
    private val mainViewModel : MainViewModelAdm by sharedViewModel()
    private val transactionViewModel : TransactionViewModel by viewModel()
    private val transactionAdapter by lazy {
        TransactionAdapter(this)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =inflater.inflate(R.layout.fragment_purchase, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        transactionViewModel.transactionResponse.observe(viewLifecycleOwner, Observer {
//            swipe_refresh_layout.isRefreshing = false
//            loading.hide()
//            Log.d(this::class.java.simpleName, it.transactions?.joinToString { "\n\n" })
            when(it.status) {
                RetrofitStatus.SUCCESS -> it.transactions?.let { list ->
                    placeholder_empty.hide()
                    recyclerview_pch.show()
                    transactionAdapter.setData(list)
                    loading.hide()
                }
                RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(requireActivity())
                else -> {
                    recyclerview_pch.hide()
                    placeholder_empty.show()
                }
            }
        })

        swipe_refresh_layout.setOnRefreshListener {
            transactionViewModel.getTransaction()
        }
        recyclerview_pch.apply {
            adapter = transactionAdapter
            layoutManager = LinearLayoutManager(recyclerview_pch.getContext(),
                RecyclerView.VERTICAL,false)
            setHasFixedSize(true)
        }

    }
    override fun onResume() {
        super.onResume()
        transactionViewModel.getTransaction()
    }
    override fun onItemClick(transaction: Transaction) {

        }
}