package com.example.bookstoreadmin.ui.book.edit

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bookstore.admin.R
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.response.book.Book
import com.bookstore.model.response.book.BookCategory
import com.bumptech.glide.Glide
import com.example.bookstoreadmin.ui.adapter.category.CategoryAdapter
import com.example.bookstoreadmin.ui.adapter.category.CategoryItemListener
import com.example.bookstoreadmin.ui.book.viewModel.DetailBookVMAdmin
import com.example.bookstoreadmin.ui.category.viewModel.EditViewModel
import com.example.bookstoreadmin.ui.main.MainActivity
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.example.utils.ViewHelper.hide
import com.example.utils.ViewHelper.show
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_edit_book.*
import kotlinx.android.synthetic.main.activity_edit_book.button_back
import kotlinx.android.synthetic.main.activity_edit_book.button_edit_book_cover
import kotlinx.android.synthetic.main.activity_edit_book.button_save
import kotlinx.android.synthetic.main.activity_edit_book.image_book_cover
import kotlinx.android.synthetic.main.fragment_book_category.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.MediaSource
import java.io.File
import java.time.LocalDateTime

class EditBookActivity : AppCompatActivity(),CategoryItemListener {
    companion object {
        const val DATA = "book_edit_activity"
        const val REQUEST_CODE_PICK_IMAGE = 101
    }
    private val mainViewModelCategory : EditViewModel by viewModel()
    private val categoryAdapter by lazy {
        CategoryAdapter(this)
    }
    private var bookKategori = listOf<BookCategory>()
    private val mainViewModel: MainViewModelAdm by viewModel()
    private val detailBookVMAdmin: DetailBookVMAdmin by viewModel()
    private var bookCategoryString = arrayListOf<String>()
    private var id:Int = 0
    private var easyImage = EasyImage.Builder(this).build()
    private lateinit var fileImages:File
    @RequiresApi(Build.VERSION_CODES.O)
    private  val current = LocalDateTime.now()
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_book)
        // access the items of the list
        val languages = resources.getStringArray(R.array.Languages)

        // access the spinner
        val spinner = findViewById<Spinner>(R.id.input_text_book_status_edit)
        if (spinner != null) {
            val adapter = ArrayAdapter(this,
                android.R.layout.simple_spinner_item, languages)
                spinner.adapter = adapter

                spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    val text: String = parent?.getItemAtPosition(position).toString()
                    input_book_status_edit.text = text
                    Toast.makeText(this@EditBookActivity,
                        getString(R.string.selected_item) + " " +
                                "" + languages[position], Toast.LENGTH_SHORT).show()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
        val spinner_1 = findViewById<Spinner>(R.id.input_text_book_category_edit)

        fun setBookCategory(){
            if (spinner_1 != null) {
                val adapter = ArrayAdapter(this,
                    android.R.layout.simple_list_item_1,  bookCategoryString)
                spinner_1.adapter = adapter

                spinner_1.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>,
                                                view: View, position: Int, id: Long) {
                        val text: String = parent?.getItemAtPosition(position).toString()
                        textViewCategoryUpdate.text = text
                        Toast.makeText(this@EditBookActivity,
                            getString(R.string.selected_item) + " " +
                                    "" +  bookCategoryString[position], Toast.LENGTH_SHORT).show()
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                        // write code to perform some action
                    }
                }
            }
        }
        mainViewModelCategory.categoryResponse.observe(this, Observer {
            when(it.status){
                RetrofitStatus.SUCCESS -> it.list?.let { list ->
                    categoryAdapter.setData(list)
                    bookKategori = list
                    bookCategoryString.clear()
                    bookKategori.map {
                        bookCategoryString.add(it.name)
                    }
                    setBookCategory()
                }
                RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(this)
                else -> {
                    recyclerview_fbc.hide()
                    placeholder_empty.show()
                }
            }
        })
        intent.getParcelableExtra<Book>(DATA).let {
                book -> if (book != null) {
                text_title.setText(book.title.trim().capitalize())
                text_authors.setText(book.authorName.trim().capitalize())
                text_price.setText(book.price.toInt().toString())
                text_synopsis.setText(book.synopsis.trim().capitalize())
                Glide.with(this).load(book.imageUrl).into(image_book_cover)
                for (i in 0..languages.size-1){
                    if (languages[i]==book.bookStatus){
                        input_text_book_status_edit.setSelection(i)
                    }
                }
                button_delete_buku.setOnClickListener{
                    detailBookVMAdmin.removeBook(book.id)
                }
                button_back.setOnClickListener {
                super.onBackPressed()
                }
                image_book_cover.setOnClickListener {
                    easyImage.openGallery(this);
                }
                button_edit_book_cover.setOnClickListener {
                  Toast.makeText(this,"${fileImages} , ${book.id}",Toast.LENGTH_LONG).show()
                   detailBookVMAdmin.uploadImage(fileImages.absoluteFile,book.id)

                }
                button_save.setOnClickListener {
                    filterKode(textViewCategoryUpdate.text.toString())
                    detailBookVMAdmin.updateBook(
                        text_authors.text.toString(),
                        id,
                        input_book_status_edit.text.toString(),
                        book.id,
                        book.isbn.toString(),
                        text_price.text.toString().toInt(),
                        current.toString(),
                        text_synopsis.text.toString(),
                        text_title.text.toString()
                    )

//                    Toast.makeText(this,"Eror 1",Toast.LENGTH_SHORT).show()
                }
            detailBookVMAdmin.addGambarResponse.observe(this, Observer {
                when(it.status){
                    RetrofitStatus.SUCCESS ->{
                        super.onBackPressed()
                        startActivity(Intent(this,MainActivity::class.java))
                    }
                    RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(this)
                    else-> Toast.makeText(this,"Eror Upload Gambar",Toast.LENGTH_SHORT).show()
                }
            })
                detailBookVMAdmin.updateBookResponse.observe(this, Observer {
                    when(it.status){
                        RetrofitStatus.SUCCESS ->{
                            super.onBackPressed()
                            startActivity(Intent(this,MainActivity::class.java))
                        }
                        RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(this)
                        else-> Toast.makeText(this,"Eror Update Book",Toast.LENGTH_SHORT).show()
                    }
                })
            detailBookVMAdmin.removeBookResponse.observe(this, Observer {
                when(it.status){
                    RetrofitStatus.SUCCESS ->{
                        super.onBackPressed()
                        startActivity(Intent(this,MainActivity::class.java))
                    }
                    RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(this)
                    else->Toast.makeText(this,"Eror 3",Toast.LENGTH_SHORT).show()
                }
            })
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        easyImage.handleActivityResult(requestCode,resultCode, data,this,object : DefaultCallback(){
            override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
                fileImages = imageFiles[0].file
                Toast.makeText(this@EditBookActivity,"${fileImages.absolutePath}",Toast.LENGTH_SHORT).show()
                Picasso.get().load(fileImages).into(image_book_cover);
            }

        })

    }
    private fun filterKode(categoryName: String?){
        for (i in 0..bookKategori.size-1){
            if(bookKategori[i].name==categoryName){
                id = bookKategori[i].id
            }
        }
    }
    override fun onItemClick(categories: BookCategory) {
        TODO("Not yet implemented")
    }
    override fun onResume() {
        super.onResume()
        mainViewModelCategory.getCategory()
    }
}