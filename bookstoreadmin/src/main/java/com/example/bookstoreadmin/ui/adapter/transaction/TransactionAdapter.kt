package com.example.bookstoreadmin.ui.adapter.transaction

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bookstore.admin.R
import com.bookstore.model.response.transaction.Transaction
import kotlinx.android.synthetic.main.item_list_purchase.view.*

class TransactionAdapter (
    private val transactionItemListener:TransactionItemListener): RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {
    private val transaction = mutableListOf<Transaction>()
    fun setData(transaction: List<Transaction>) {
        this.transaction.clear()
        this.transaction.addAll(transaction.sortedBy { it.id })
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_list_purchase, parent, false)
    )

    override fun getItemCount(): Int = transaction.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(transaction[position])


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("DefaultLocale")
        fun bind(transaction: Transaction) {
            itemView.text_name_pch.text = transaction.invoiceNumber.trim().capitalize()
            itemView.tanggalPU.text = transaction.createdTime.trim().capitalize()
            itemView.card_pch.setOnClickListener {
                transactionItemListener.onItemClick(transaction)
            }
        }
    }

}
