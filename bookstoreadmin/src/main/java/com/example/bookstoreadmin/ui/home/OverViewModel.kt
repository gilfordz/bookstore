package com.example.bookstoreadmin.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.formatted.book.BookResponse
import com.example.bookstoreadmin.model.formatted.category.CategoryResponse
import com.example.bookstoreadmin.repository.BookRespository
import com.example.bookstoreadmin.repository.CateRepository
import com.example.utils.Retrofit.printRetrofitError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException

class OverViewModel(
    application: Application,
    private val bookRespository: BookRespository,
    private val cateRepository: CateRepository
):AndroidViewModel(application) {
    private val _bookResponse = MutableLiveData<BookResponse>()
    val bookresponse: LiveData<BookResponse> = _bookResponse

    fun getCountBook() = viewModelScope.launch(Dispatchers.IO) {
        try {
            val result = bookRespository.getBook()
            _bookResponse.postValue(BookResponse(RetrofitStatus.SUCCESS,result))

        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _bookResponse.postValue(BookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _bookResponse.postValue(BookResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }

    private val _categoryResponse = MutableLiveData<CategoryResponse>()
    val categoryResponse: LiveData<CategoryResponse> = _categoryResponse
    fun getCountCategory() = viewModelScope.launch(Dispatchers.IO) {
        try {
            val result = cateRepository.getCategories()
            _categoryResponse.postValue(CategoryResponse(RetrofitStatus.SUCCESS,result))

        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _categoryResponse.postValue(CategoryResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _categoryResponse.postValue(CategoryResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
}