package com.example.bookstoreadmin.ui.main.fragment.purchase

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bookstore.constant.RetrofitStatus
import com.example.bookstoreadmin.model.formatted.transaction.TransactionResponse
import com.example.bookstoreadmin.repository.TransactionRepository
import com.example.utils.Retrofit.printRetrofitError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException

class TransactionViewModel(
    application: Application,
    private val transactionRepository: TransactionRepository
):AndroidViewModel(application) {
    private val _transResponse = MutableLiveData<TransactionResponse>()
    val transactionResponse:LiveData<TransactionResponse> = _transResponse

    fun getTransaction() = viewModelScope.launch ( Dispatchers.IO){
        try {
            val result = transactionRepository.getTransaction().sortedBy { it.id }
            if (result.isNotEmpty()) _transResponse.postValue(TransactionResponse(RetrofitStatus.SUCCESS, result))
            else{
                _transResponse.postValue(TransactionResponse(RetrofitStatus.EMPTY))
                Log.e(this::class.java.simpleName,result.toString())
            }
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _transResponse.postValue(TransactionResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _transResponse.postValue(TransactionResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
}