package com.example.bookstoreadmin.ui.category.edit

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bookstore.admin.R
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.response.book.BookCategory
import com.example.bookstoreadmin.ui.category.viewModel.EditViewModel
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.dialog_edit_book_category.*
import kotlinx.android.synthetic.main.fragment_book_category.*
import org.koin.android.viewmodel.ext.android.viewModel

class EditCategories(private val intent: Intent) : BottomSheetDialogFragment() {
    companion object{
        const val DATA = "book_edit_activity_data"
    }
    private val mainViewModel:MainViewModelAdm by viewModel()
    private val editViewModel: EditViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_edit_book_category,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
           intent.getParcelableExtra<BookCategory>(DATA).let{
            bookCategory -> if(bookCategory!=null){
            button_save.setOnClickListener {
                editViewModel.updateBookCategories(bookCategory.id,text_code_bookcategory.text.toString(),text_name_bookcategory.text.toString())
                dialog?.dismiss()
                dialog?.cancel()
            }
            button_delete.setOnClickListener {
                editViewModel.removeFromCategory(bookCategory.id)
                dialog?.dismiss()
                dialog?.cancel()
            }
               button_cancel.setOnClickListener {
                   dialog?.dismiss()
                   dialog?.cancel()
               }
            text_name_bookcategory.setText(bookCategory.name.trim().capitalize())
            text_code_bookcategory.setText(bookCategory.code.trim().capitalize())
            editViewModel.updateBookCategoryResponse.observe(this, androidx.lifecycle.Observer {
                when(it.status){
                    RetrofitStatus.SUCCESS ->{
                        dialog?.dismiss()
                        dialog?.cancel()
                    }
                    else-> showSnackbar("Error occured when update category")
                }
            })
            editViewModel.removeCategoryResponse.observe(this, Observer {
                when(it.status){
                    RetrofitStatus.SUCCESS ->{
                        dialog?.dismiss()
                        dialog?.cancel()
                    }
                    else-> showSnackbar("Error occured when delete category")
                }
            })

        }
        }
    }
    private fun showSnackbar(msg:String) = Snackbar.make(parent_layout,msg, Snackbar.LENGTH_SHORT).show()
}