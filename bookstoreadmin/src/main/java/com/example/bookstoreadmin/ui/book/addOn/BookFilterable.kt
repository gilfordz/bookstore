package com.example.bookstoreadmin.ui.book.addOn

interface BookFilterable {
    fun performFilterByName(bookName:String?)
}