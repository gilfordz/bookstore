package com.example.bookstoreadmin.ui.adapter.category

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bookstore.admin.R
import com.bookstore.model.response.book.Book
import com.bookstore.model.response.book.BookCategory
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_add_book.view.*
import kotlinx.android.synthetic.main.item_list_book.view.*

class CategoryAdapter(
    private val categoryItemListener: CategoryItemListener
): RecyclerView.Adapter<CategoryAdapter.ViewHolder>(){
    private val categories = mutableListOf<BookCategory>()
    private var kategori = listOf<BookCategory>()

    fun setData(categorie:List<BookCategory>){
        this.categories.clear()
        kategori = categorie
        this.categories.addAll(categorie)
        notifyDataSetChanged()
    }
override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
    ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list_category, parent, false ))

override fun getItemCount(): Int {
    return kategori.size
}

    @SuppressLint("DefaultLint")
    fun performFilterByName(categoryName: String?) {
        kategori = categories
        if (!categoryName.isNullOrEmpty()) kategori = categories.filter {
            it.name.trim().toLowerCase().contains(categoryName.trim().toLowerCase())
        }
        kategori.sortedBy { it.id }
//        bookItemListener.onItemSearch(bookz.isEmpty())
        notifyDataSetChanged()
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(kategori[position])
inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
    @SuppressLint("DefaultLocale")
    fun bind(categories: BookCategory) {
        Glide.with(itemView.context)
        itemView.text_name.text = categories.name.trim().capitalize()
        itemView.card.setOnClickListener {
            categoryItemListener.onItemClick(categories)
        }
    }

}

}