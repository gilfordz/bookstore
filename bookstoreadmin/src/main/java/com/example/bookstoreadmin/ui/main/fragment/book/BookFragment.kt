package com.example.bookstoreadmin.ui.main.fragment.book

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import com.example.utils.ViewHelper.showKeyboard
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bookstore.admin.R
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.response.book.Book
import com.example.bookstoreadmin.ui.book.add.AddBookActivity
import com.example.bookstoreadmin.ui.book.viewModel.DetailBookVMAdmin
import com.example.bookstoreadmin.ui.book.edit.EditBookActivity
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.example.ui.book.adapter.BookAdapter
import com.example.ui.book.adapter.BookItemListener
import com.example.utils.ViewHelper.hide
import com.example.utils.ViewHelper.hideKeyboard
import com.example.utils.ViewHelper.show
import kotlinx.android.synthetic.main.fragment_book.*
import kotlinx.android.synthetic.main.fragment_book.loading
import kotlinx.android.synthetic.main.fragment_book.placeholder_empty
import kotlinx.android.synthetic.main.fragment_book.swipe_refresh_layout
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class BookFragment : Fragment(),BookItemListener {
    private val mainViewModel: MainViewModelAdm by sharedViewModel()
    private val bookViewModel: DetailBookVMAdmin by viewModel()
    private val bookAdapter by lazy {
        BookAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_book, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bookViewModel.bookresponse.observe(viewLifecycleOwner, Observer {
            swipe_refresh_layout.isRefreshing = false
            loading.hide()
            when(it.status) {
                RetrofitStatus.SUCCESS -> it.list?.let { list ->
                    placeholder_empty.hide()
                    recyclerview.show()
                    bookAdapter.setData(list)
                    text_book_count.text = list.size.toString()
                }
                RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(requireActivity())
                else -> {
                    recyclerview.hide()
                    placeholder_empty.show()
                }
            }
        })
        button_add.setOnClickListener {
            startActivity(Intent(requireContext(), AddBookActivity::class.java))
        }
        button_search.setOnClickListener {
           input_search.show()
           input_search.showKeyboard()
            text_title_fragment_book.hide()
            button_search.hide()

        }
        input_search.setOnEditorActionListener{ _,actionid,_->
            if (actionid == EditorInfo.IME_ACTION_SEARCH){
                bookAdapter.performFilterByName(input_search.text.toString())
                input_search.hideKeyboard()
            }
            true
        }

        swipe_refresh_layout.setOnRefreshListener {
            bookViewModel.getBook()
        }
        recyclerview.apply {
            adapter = bookAdapter
            layoutManager = LinearLayoutManager(recyclerview.getContext(),RecyclerView.VERTICAL,false)
            setHasFixedSize(true)
        }

    }
    override fun onResume() {
        super.onResume()
        bookViewModel.getBook()
    }

    override fun onItemClick(book: Book) {
        val intent = Intent(requireContext(), EditBookActivity::class.java)
        startActivity(intent.putExtra(EditBookActivity.DATA, book))
    }

}