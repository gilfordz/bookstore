package com.example.dao.remote
import com.bookstore.model.response.transaction.Transaction
import retrofit2.http.GET
import retrofit2.http.Header

interface RemoteTransactionDAO {
    @GET("/api/rest/transaction/findAll")
    suspend fun getTransaction(@Header("Authorization") authorization : String) : List<Transaction>

}