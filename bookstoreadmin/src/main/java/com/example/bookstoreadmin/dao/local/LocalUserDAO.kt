package com.example.dao.local

import androidx.room.*
import com.bookstore.model.response.user.AccessToken
import com.example.config.AppConfig

@Dao
interface LocalUserDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveAccessToken(accessToken: AccessToken):Long

    @Query("Select *from ${AppConfig.ROOM_DEFAULT_SESSION_TABLE_NAME} Where  id=0")
    suspend fun getCurrentAccessToken():AccessToken?

    @Delete
    suspend fun removeCurrentAccessToken(accessToken: AccessToken):Int
}