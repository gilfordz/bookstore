package com.example.bookstoreadmin.dao.remote

import com.bookstore.model.response.book.BookCategory
import com.example.bookstoreadmin.model.request.category.CategoryBookRequest
import com.example.config.AppConfig
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface RemoteCategoryDAO {
    @DELETE("/api/rest/book-category/deleteById/{id}")
    suspend fun removeFromCategory(
        @Header("Authorization") authorization: String,
        @Path("id") id:Int
    ) : Response<ResponseBody>

    @GET("/api/rest/book-category/findAll")
    suspend fun getCategory(@Header("Authorization") authorization : String) : List<BookCategory>

    @GET("/api/rest/book-category/findByUserId/${AppConfig.OAUTH_DEFAULT_CUSTOMER_ID}")
    suspend fun getPeaceCategory(@Header("Authorization") authorization: String) : BookCategory

    @POST("/api/rest/book-category/save")
    suspend fun addBookCategories(
        @Header("Authorization") authorization: String,
        @Body categoryBookRequest: CategoryBookRequest
    ) : Response<ResponseBody>

    @POST("/api/rest/book-category/update")
    suspend fun updateBookCategories(
        @Header("Authorization") authorization: String,
        @Body categoryBookRequest: CategoryBookRequest
    ) : Response<ResponseBody>


}