package com.example.dao.remote

import com.bookstore.model.response.book.Book
import com.example.bookstoreadmin.model.request.book.BookRequest
import com.example.config.AppConfig
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface RemoteBookDAO {
    @GET("/api/rest/book/findAll")
    suspend fun getBook(@Header("Authorization") authorization : String) : List<Book>
    @GET("/api/rest/book/findByUserId/${AppConfig.OAUTH_DEFAULT_CUSTOMER_ID}")
    suspend fun getOneBook(@Header("Authorization") authorization: String) : List<Book>
    @POST("/api/rest/book/update")
    suspend fun updateBook(
        @Header("Authorization") authorization: String,
        @Body bookRequest: BookRequest
    ) : Response<ResponseBody>
    @POST("/api/rest/book/save")
    suspend fun addBook(
        @Header("Authorization") authorization: String,
        @Body BookRequest: BookRequest
    ) : Response<ResponseBody>
    @POST("/api/rest/book/uploadImage/{id}")
    @Multipart
       suspend fun uploadImage(
        @Header("Authorization") authorization: String,
        @Part file: MultipartBody.Part,
        @Path("id") id:Int
    ) : Response<ResponseBody>
    @DELETE("/api/rest/book/deleteById/{id}")
    suspend fun removeBook(
        @Header("Authorization") authorization: String,
        @Path("id") id:Int
    ) : Response<ResponseBody>
}