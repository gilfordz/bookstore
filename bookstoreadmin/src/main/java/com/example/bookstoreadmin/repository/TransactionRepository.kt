package com.example.bookstoreadmin.repository

import com.bookstore.model.response.transaction.Transaction
import com.example.dao.remote.RemoteTransactionDAO
import com.example.repository.AdmRepository
import com.example.utils.SessionHelper
import com.example.utils.SessionHelper.asBearer

class TransactionRepository(
    private val admRepository : AdmRepository,
    private val transactionDAO : RemoteTransactionDAO
) {
    suspend fun getTransaction() : List<Transaction> = admRepository.checkSession().let {
        if (it != null) return transactionDAO.getTransaction(it.asBearer())
        else throw SessionHelper.unauthorozation
    }
}