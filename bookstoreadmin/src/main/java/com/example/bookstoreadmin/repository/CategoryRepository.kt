package com.example.bookstoreadmin.repository

import com.bookstore.model.response.book.BookCategory
import com.example.bookstoreadmin.dao.remote.RemoteCategoryDAO
import com.example.bookstoreadmin.model.request.category.CategoryBookRequest
import com.example.repository.AdmRepository
import com.example.utils.SessionHelper
import com.example.utils.SessionHelper.asBearer
import okhttp3.ResponseBody
import retrofit2.Response

class CateRepository(
    private val admRepository: AdmRepository,
    private val categoryDAO: RemoteCategoryDAO
) {
    suspend fun getCategories() : List<BookCategory> = admRepository.checkSession().let {
        if(it !=null) return categoryDAO.getCategory(it.asBearer())
        else throw SessionHelper.unauthorozation
    }
    suspend fun addBookToCategory(categoryBookRequest: CategoryBookRequest) : Response<ResponseBody> =
        admRepository.checkSession().let {
            if (it != null) return categoryDAO.addBookCategories(it.asBearer(),categoryBookRequest)
            else throw SessionHelper.unauthorozation
        }
    suspend fun updateBookCategories(categoryBookRequest: CategoryBookRequest) : Response<ResponseBody> =
        admRepository.checkSession().let {
            if (it != null) return categoryDAO.updateBookCategories(it.asBearer(),categoryBookRequest)
            else throw SessionHelper.unauthorozation
        }
    suspend fun removeFromCategory(id:Int) : Response<ResponseBody> =
        admRepository.checkSession().let {
            if (it !=null) return categoryDAO.removeFromCategory(it.asBearer(),id)
            else throw SessionHelper.unauthorozation
        }
}