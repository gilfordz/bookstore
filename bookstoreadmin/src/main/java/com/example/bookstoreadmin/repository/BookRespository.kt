package com.example.bookstoreadmin.repository

import com.bookstore.model.response.book.Book
import com.example.bookstoreadmin.model.request.book.BookRequest
import com.example.dao.remote.RemoteBookDAO
import com.example.repository.AdmRepository
import com.example.utils.SessionHelper
import com.example.utils.SessionHelper.asBearer
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.File

class BookRespository (
    private val userRepository : AdmRepository,
            private val bookDAO : RemoteBookDAO )
{
    suspend fun getBook() : List<Book> = userRepository.checkSession().let {
        if(it != null) return bookDAO.getBook(it.asBearer())
        else  throw SessionHelper.unauthorozation
    }
    suspend fun getOneBook() : List<Book> = userRepository.checkSession().let {
        if(it != null) return bookDAO.getOneBook(it.asBearer())
        else throw SessionHelper.unauthorozation
    }
    suspend fun updateBook(bookRequest: BookRequest) : Response<ResponseBody> =
        userRepository.checkSession().let {
        if(it !=null) return bookDAO.updateBook(it.asBearer(),bookRequest)
        else throw SessionHelper.unauthorozation
    }
    suspend fun addBook(bookRequest: BookRequest) : Response<ResponseBody> =
        userRepository.checkSession().let {
            if (it != null) return bookDAO.addBook(it.asBearer(),bookRequest)
            else throw SessionHelper.unauthorozation
        }
    suspend fun removeBook(id:Int) : Response<ResponseBody> =
        userRepository.checkSession().let {
            if (it !=null) return bookDAO.removeBook(it.asBearer(),id)
            else throw SessionHelper.unauthorozation
        }
    suspend fun uploadImage(file: File, id: Int) : Response<ResponseBody> =
        userRepository.checkSession().let {
            val formattedImage = MultipartBody.Part.createFormData(
                "file",
                file.name,
                RequestBody.create(MediaType.parse("image/*"), file)
            )
            if (it !=null) return bookDAO.uploadImage(it.asBearer(),formattedImage,id)
            else throw SessionHelper.unauthorozation
        }

}