package com.example.database.remote

import com.example.bookstoreadmin.dao.remote.RemoteCategoryDAO
import com.example.dao.remote.RemoteBookDAO
import com.example.dao.remote.RemoteTransactionDAO
import com.example.dao.remote.RemoteUserDAO
import com.example.utils.Retrofit

object RemoteDatabase {
val userDAO: RemoteUserDAO = Retrofit.getClient().create(RemoteUserDAO::class.java)
val bookDAO: RemoteBookDAO = Retrofit.getClient().create(RemoteBookDAO::class.java)
val transactionDAO: RemoteTransactionDAO = Retrofit.getClient().create(RemoteTransactionDAO::class.java)
val categoryDAO: RemoteCategoryDAO = Retrofit.getClient().create(RemoteCategoryDAO::class.java)
}