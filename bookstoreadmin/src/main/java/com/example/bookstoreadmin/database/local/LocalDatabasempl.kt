package com.example.database.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.bookstore.model.response.user.AccessToken
import com.example.config.AppConfig
import com.example.dao.local.LocalUserDAO
import org.koin.core.KoinComponent
import org.koin.core.inject


@Database(entities = [AccessToken::class],version = 1,exportSchema = false)
internal abstract class LocalDatabasempl : RoomDatabase(){

    internal abstract val userDAO : LocalUserDAO

    companion object : KoinComponent {
        private val context:Context by inject()

        internal val database =
            Room.databaseBuilder(
                context.applicationContext,
                LocalDatabasempl::class.java,
                AppConfig.ROOM_DEFAULT_DATABASE_NAME
            ).fallbackToDestructiveMigration().build()
    }
}