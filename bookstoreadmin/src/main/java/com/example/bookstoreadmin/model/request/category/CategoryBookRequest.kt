package com.example.bookstoreadmin.model.request.category

data class CategoryBookRequest(
    val id:Int,
    val code:String,
    val name:String
)