package com.example.bookstoreadmin.model.formatted.category

import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.response.book.BookCategory

class CategoryResponse (
    val status: RetrofitStatus = RetrofitStatus.UNKNOWN,
            val list:List<BookCategory>? = null
)