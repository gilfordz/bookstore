package com.example.bookstoreadmin.model.request.book
data class BookRequest(
    val authorName: String,
    val bookCategoryId: Int,
    val bookStatus: String,
    val id: Int,
    val isbn: String,
    val price: Int,
    val publicationDate: String,
    val synopsis: String,
    val title: String
)