package com.example.bookstoreadmin.model.formatted.transaction

import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.response.transaction.Transaction

class TransactionResponse (
    val status: RetrofitStatus = RetrofitStatus.UNKNOWN,
    val transactions: List<Transaction>? = null
)