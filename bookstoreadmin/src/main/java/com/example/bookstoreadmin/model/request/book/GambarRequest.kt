package com.example.bookstoreadmin.model.request.book
import java.io.File
data class GambarRequest(
    val file: File,
    val id: Int
)
