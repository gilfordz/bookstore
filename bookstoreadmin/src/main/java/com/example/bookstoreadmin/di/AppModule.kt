package com.example.bookstoreadmin.di

import com.example.bookstoreadmin.repository.BookRespository
import com.example.bookstoreadmin.repository.CateRepository
import com.example.bookstoreadmin.repository.TransactionRepository
import com.example.bookstoreadmin.signin.SignInViewModelAdm
import com.example.bookstoreadmin.ui.book.viewModel.DetailBookVMAdmin
import com.example.bookstoreadmin.ui.category.viewModel.EditViewModel
import com.example.bookstoreadmin.ui.main.MainViewModelAdm
import com.example.bookstoreadmin.ui.home.OverViewModel
import com.example.bookstoreadmin.ui.main.fragment.purchase.TransactionViewModel
import com.example.bookstoreadmin.ui.splashscreen.SplashScreenViewModel
import com.example.database.local.LocalDatabase
import com.example.database.remote.RemoteDatabase
import com.example.repository.AdmRepository
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    // DAO
    single { LocalDatabase.userDAO}
    single { RemoteDatabase.userDAO }
    single { RemoteDatabase.bookDAO }
    single { RemoteDatabase.transactionDAO }
    single { RemoteDatabase.categoryDAO }

    // Repository
    single { AdmRepository(get(),get()) }
    single { BookRespository(get(),get()) }
    single { CateRepository(get(),get()) }
    single { TransactionRepository(get(),get())}

    // View Model
    viewModel { SplashScreenViewModel(get(),get()) }
    viewModel { SignInViewModelAdm(get(),get()) }
    viewModel { MainViewModelAdm(get(),get()) }
    viewModel {
        TransactionViewModel(
            get(),
            get()
        )
    }
    viewModel {
        EditViewModel(
            get(),
            get()
        )
    }
    viewModel {
        DetailBookVMAdmin(
            get(),
            get()
        )
    }
    viewModel {
        OverViewModel(
            get(),
            get(),
            get()
        )
    }
}