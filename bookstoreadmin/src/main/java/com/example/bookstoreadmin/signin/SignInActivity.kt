package com.example.bookstoreadmin.signin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.bookstore.admin.R
import com.bookstore.constant.RetrofitStatus
import com.example.bookstoreadmin.ui.main.MainActivity
import com.example.config.AppConfig
import com.example.utils.ViewHelper.invisible
import com.example.utils.ViewHelper.show
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_signin.*
import org.koin.android.viewmodel.ext.android.viewModel

class SignInActivity : AppCompatActivity() {
    private val signInViewModel : SignInViewModelAdm by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        signInViewModel.signInResponse.observe(this, Observer {
            if(it.status == RetrofitStatus.SUCCESS){
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }else{
                loading.invisible()
                button_signin.isEnabled = true
                Snackbar.make(parent_layout,"There is in Error in Sign In", Snackbar.LENGTH_SHORT).show()
            }
        })
    }

    override fun onStart() {
        super.onStart()
        button_signin.setOnClickListener{
            val username = input_username.editText?.text.toString()
            val password = input_password.editText?.text.toString()
            input_username.error = if (username.isEmpty()) "PLease type your username" else null
            input_password.error = if (password.isEmpty()) "PLease type your password " else null
            if (username== AppConfig.OAUTH_DEFAULT_ACCOUNT_USERNAME && password == AppConfig.OAUTH_DEFAULT_ACCOUNT_PASSWORD){
                loading.show()
                button_signin.isEnabled == false
                signInViewModel.signIn(username.trim(), password.trim())
            }else{
                input_username.error = if (username != AppConfig.OAUTH_DEFAULT_ACCOUNT_USERNAME) "We Cant find your username" else null
                input_password.error = if (password != AppConfig.OAUTH_DEFAULT_ACCOUNT_USERNAME) "We Cant find your password " else null
                Snackbar.make(parent_layout,"your Username & Password are wrong, check again please",
                    Snackbar.LENGTH_SHORT).show()
            }

        }    }
}