package com.example.bookstoreadmin.signin

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.formatted.user.SignInResponse
import com.bookstore.model.request.user.AccessTokenRequest
import com.example.repository.AdmRepository
import com.example.utils.Retrofit.printRetrofitError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SignInViewModelAdm(
    application: Application,
    private val admRepository: AdmRepository
): AndroidViewModel(application) {
    private val _signInResponse = MutableLiveData<SignInResponse>()
    val signInResponse: LiveData<SignInResponse> = _signInResponse

    fun signIn(username:String,password:String) = viewModelScope.launch(Dispatchers.IO){
        try {
            val accessTokenRequest = AccessTokenRequest(username = username , password = password)
            admRepository.getAccessToken(accessTokenRequest).run {
                admRepository.saveSession(this)
                _signInResponse.postValue(SignInResponse(RetrofitStatus.SUCCESS))
            }

        }catch (throwable:Throwable){
            _signInResponse.postValue(SignInResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
}