package com.example.ui.search

import com.bookstore.constant.BookType
import com.bookstore.model.response.book.Book

interface SearchBookItemListener {

    fun onItemSearch(empty:Boolean)

    fun onItemClick(book: Book)

    fun onFilterByType(bookTypes:List<BookType>)
}