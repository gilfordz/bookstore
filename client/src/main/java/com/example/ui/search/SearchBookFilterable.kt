package com.example.ui.search

import com.bookstore.constant.BookType

interface SearchBookFilterable {
    fun performFilterByName(bookName:String?,bookTypes:List<BookType>)

    fun performFilterByType(bookTypes:List<BookType>)
}