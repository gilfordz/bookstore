package com.example.ui.book

import android.app.Application
import android.service.voice.VoiceInteractionSession
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bookstore.constant.CartStatus
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.formatted.book.BookResponse
import com.bookstore.model.formatted.book.FavouriteBookResponse
import com.bookstore.model.formatted.cart.CartResponse
import com.bookstore.model.request.book.FavouriteBookRequest
import com.bookstore.model.request.cart.CartRequest
import com.bookstore.model.response.book.Book
import com.example.repository.BookRespository
import com.example.repository.CartRepository
import com.example.utils.Retrofit.printRetrofitError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException

class DetailBookViewModel(
    application: Application,
    private val cartRepository: CartRepository,
    private val bookRespository: BookRespository
):AndroidViewModel(application) {
    var currentBook: Book? = null
    private val _cartResponse = MutableLiveData<CartResponse>()
    val cartResponse:LiveData<CartResponse> = _cartResponse
    var cartAdded = false

    fun getCart(bookId: Int) = viewModelScope.launch(Dispatchers.IO){
        try {
            val result = cartRepository.getCart()
            result.details.firstOrNull{
           it.bookModel.id == bookId
            }?.let { if (it.cartDetailStatus == CartStatus.CARTED.toString()) cartAdded = true }
            _cartResponse.postValue(CartResponse(RetrofitStatus.SUCCESS,result))
        }catch (thworable:Throwable){
            if (thworable is HttpException && thworable.code() == 401)
                _cartResponse.postValue(CartResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _cartResponse.postValue(CartResponse(RetrofitStatus.FAILURE))
            thworable.printRetrofitError()
        }
    }
    private val _addCartResponse = MutableLiveData<CartResponse>()
    val addCartResponse:LiveData<CartResponse> = _addCartResponse

    fun addBookToCart(bookId:Int) = viewModelScope.launch(Dispatchers.IO){
        try {
            val cartRequest = CartRequest(bookId)
            val result =  cartRepository.addBookToCart(cartRequest)
            if (result.isSuccessful){
                cartAdded = true
                _addCartResponse.postValue(CartResponse(RetrofitStatus.SUCCESS))
            }else{
                _addCartResponse.postValue(CartResponse(RetrofitStatus.FAILURE))
                Log.e(this::class.java.simpleName,result.toString())
            }
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _addCartResponse.postValue(CartResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _addCartResponse.postValue(CartResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
    private val _removeCartResponse = MutableLiveData<CartResponse>()
    val removeCartResponse:LiveData<CartResponse> = _removeCartResponse

    fun removeBookFromCart(bookId:Int) = viewModelScope.launch(Dispatchers.IO){
        try {
            val cart = cartRepository.getCart()
            cart.details.firstOrNull{
                it.bookModel.id == bookId}?.id.let { detailId->
                if(detailId !=null){
                    val result = cartRepository.removeFromCart(detailId)
                    if (result.isSuccessful){
                        cartAdded = false
                        _removeCartResponse.postValue(CartResponse(RetrofitStatus.SUCCESS))
                    }else{
                        _removeCartResponse.postValue(CartResponse(RetrofitStatus.FAILURE))
                        Log.e(this::class.java.simpleName, result.toString())
                    }
            }else{
                    _removeCartResponse.postValue(CartResponse(RetrofitStatus.FAILURE))
                    Log.e(this::class.java.simpleName,"Cant find book id : $bookId in cart data")
                }
            }
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _addCartResponse.postValue(CartResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _addCartResponse.postValue(CartResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }

    private  val _proceedToCheckoutResponse = MutableLiveData<CartResponse>()
    val proceedToCheckoutResponse:LiveData<CartResponse> = _proceedToCheckoutResponse
    fun proceedToCheckout(bookId:Int) = viewModelScope.launch(Dispatchers.IO){
        try {
            val cartRequest = CartRequest(bookId)
            val result =  cartRepository.addBookToCart(cartRequest)
            if (result.isSuccessful){
                cartAdded = true
                _proceedToCheckoutResponse.postValue(CartResponse(RetrofitStatus.SUCCESS))
            }else{
                _proceedToCheckoutResponse.postValue(CartResponse(RetrofitStatus.FAILURE))
                Log.e(this::class.java.simpleName,result.toString())
            }
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _proceedToCheckoutResponse.postValue(CartResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _proceedToCheckoutResponse.postValue(CartResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }

    private  val _favoriteBookResponse = MutableLiveData<FavouriteBookResponse>()
    val favouriteBookResponse:LiveData<FavouriteBookResponse> = _favoriteBookResponse
    var favouriteBookAdded = false

    fun getFavoriteBook(bookId:Int) = viewModelScope.launch(Dispatchers.IO){
        try {
            val result =  bookRespository.getFavouriteBook()
            if (result.details.firstOrNull{it.bookModel.id == bookId}!==null) favouriteBookAdded = true
                _favoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.SUCCESS,result))
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _favoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _favoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
    private  val _addFavoriteBookResponse = MutableLiveData<FavouriteBookResponse>()
    val addFavouriteBookResponse:LiveData<FavouriteBookResponse> = _addFavoriteBookResponse

    fun addBookToFavorite(bookId:Int) = viewModelScope.launch(Dispatchers.IO){
        try {
            val favouriteBookRequest = FavouriteBookRequest(bookId)
            val result =  bookRespository.getBookToBook(favouriteBookRequest)
            if (result.isSuccessful) favouriteBookAdded = true
            _addFavoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.SUCCESS))
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _addFavoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _addFavoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
    private val _removeFavoriteBookResponse = MutableLiveData<FavouriteBookResponse>()
    val removeFavouriteBookResponse:LiveData<FavouriteBookResponse> = _removeFavoriteBookResponse

    fun removeBookFromFavorite(bookId:Int) = viewModelScope.launch(Dispatchers.IO){
        try {
            val favorites = bookRespository.getFavouriteBook()
            favorites.details.firstOrNull{
                it.bookModel.id == bookId}?.id.let { detailId->
                if(detailId !=null){
                    val result = bookRespository.removeBookFromFavourite(detailId)
                    if (result.isSuccessful){
                        favouriteBookAdded = false
                        _removeFavoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.SUCCESS))
                    }else{
                        _removeFavoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.FAILURE))
                        Log.e(this::class.java.simpleName, result.toString())
                    }
                }else{
                    _removeFavoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.FAILURE))
                    Log.e(this::class.java.simpleName,"Cant find book id : $bookId in cart data")
                }
            }
        }catch (throwable:Throwable){
            if (throwable is HttpException && throwable.code() == 401)
                _removeFavoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _removeFavoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.FAILURE))
            throwable.printRetrofitError()
        }
    }
}