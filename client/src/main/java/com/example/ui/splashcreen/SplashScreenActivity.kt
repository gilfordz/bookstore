package com.example.ui.splashcreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bookstore.constant.SessionStatus
import com.example.signin.ActivitySignin
import com.example.ui.main.MainActivity
import org.koin.android.viewmodel.ext.android.viewModel

class SplashScreenActivity : AppCompatActivity() {
    private val splashScreenViewModel : SplashScreenViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        splashScreenViewModel.sessionResponse.observe(this, Observer {
            if (it.status == SessionStatus.AVAILABLE) startActivity(Intent(this,MainActivity::class.java))
            else startActivity(Intent(this, ActivitySignin::class.java))
            finish()
        })
    }

    override fun onStart() {
        super.onStart()
        splashScreenViewModel.checkSession()
    }
}