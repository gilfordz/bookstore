package com.example.ui.main.fragment.cart

interface CartFilterable {
    fun performFilterByName(bookName:String?)
}