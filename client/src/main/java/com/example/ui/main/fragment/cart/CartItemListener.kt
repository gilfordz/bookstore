package com.example.ui.main.fragment.cart

import com.bookstore.model.response.cart.CartDetail
import java.text.FieldPosition

interface CartItemListener {
    fun onItemSearch(empty:Boolean)

    fun onItemClick(cartDetail: CartDetail)

    fun onItemRemove(position: Int,cartDetail: CartDetail)

    fun onItemDraw(cartDetail: List<CartDetail>)
}