package com.example.ui.checkout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import com.bookstore.constant.RetrofitStatus
import com.example.bookstore.R
import com.example.ui.checkout.fragment.CheckoutFragment
import com.example.ui.checkout.fragment.payment.PaymentFragment
import com.example.ui.checkout.fragment.payment.PaymentSuccessFragment
import com.example.ui.main.MainViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_checkout.*
import org.koin.android.viewmodel.ext.android.viewModel

class CheckoutActivity : AppCompatActivity() {
    private val mainViewModel:MainViewModel by viewModel()
    private val checkoutViewModel:CheckoutViewModel by viewModel()
    private val checkoutFragment by lazy {
        CheckoutFragment()
    }
    private val paymentFragment by lazy {
        PaymentFragment()
    }
    private val paymentSuccessFragment by lazy {
        PaymentSuccessFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)
        checkoutViewModel.checkoutResponse.observe(this, Observer {
            cart -> when(cart.status){
            RetrofitStatus.SUCCESS -> Log.d(this::class.java.simpleName,"Successfully fetching cart data")
            RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(this)
            else -> Log.e(this::class.java.simpleName,"Error occured when fetching cart data")
        }
        })
        checkoutViewModel.checkoutResponse.observe(this, Observer {
            checkout -> when(checkout.status){
            RetrofitStatus.SUCCESS -> checkout.transaction?.let {
                checkoutTransaction -> paymentFragment.setData(checkoutTransaction)
                supportFragmentManager.commit {
                    setCustomAnimations(R.anim.slide_in_right,R.anim.slide_in_left)
                    replace(fragment_container.id,paymentFragment)
                }
            }
            RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(this)
            else -> Log.e(this::class.java.simpleName,"Error occurred when performing checkout")
        }
        })
        checkoutViewModel.paymentResponse.observe(this, Observer {
            payment -> when(payment.status){
            RetrofitStatus.SUCCESS -> supportFragmentManager.commit {
                setCustomAnimations(R.anim.slide_in_right,R.anim.slide_in_left)
                replace(fragment_container.id,paymentSuccessFragment)
            }
            RetrofitStatus.UNAUTHORIZED -> mainViewModel.logout(this)
            else -> Log.e(this::class.java.simpleName,"Error occored when performing payment")
        }
        })
        supportFragmentManager.commit {
            setCustomAnimations(R.anim.slide_in_right,R.anim.slide_in_left)
            replace(fragment_container.id,checkoutFragment)
        }
        button_back.setOnClickListener {
            onBackPressed()
        }

    }

    override fun onBackPressed() {
       MaterialAlertDialogBuilder(this)
           .setMessage(getString(R.string.dialog_cancel_payment))
           .setPositiveButton(getString(R.string.button_yes)){_,_->super.onBackPressed()}
           .setNegativeButton(getString(R.string.button_cancel)){dialog,_ -> dialog.dismiss() }
               .show()
    }
}