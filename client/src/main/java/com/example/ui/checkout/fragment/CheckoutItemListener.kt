package com.example.ui.checkout.fragment

import com.bookstore.model.response.cart.CartDetail

interface CheckoutItemListener {
    fun onItemClick(cartDetail: CartDetail)
    fun onItemDraw(cartDetail: List<CartDetail>)
}