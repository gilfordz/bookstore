package com.example.ui.wishlist

import com.bookstore.model.response.book.Book

interface WishlistItemListener {
    fun onItemSearch(empty:Boolean)

    fun onItemClick(book: Book)
}