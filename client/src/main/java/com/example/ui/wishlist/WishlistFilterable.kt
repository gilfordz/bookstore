package com.example.ui.wishlist

interface WishlistFilterable {
    fun performFilterByName(bookName:String?)
}