package com.example.ui.wishlist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bookstore.constant.RetrofitStatus
import com.bookstore.model.formatted.book.FavouriteBookResponse
import com.example.repository.BookRespository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException

class WishlistViewModel(
    application: Application,
    private val  bookRespository: BookRespository
):AndroidViewModel(application) {

    private  val _favoriteBookResponse = MutableLiveData<FavouriteBookResponse>()
    val favouriteBookResponse:LiveData<FavouriteBookResponse> = _favoriteBookResponse

    fun getFavoriteBook() = viewModelScope.launch(Dispatchers.IO){
        try {
            val result = bookRespository.getFavouriteBook()
            if(result.details.isNotEmpty())
                _favoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.SUCCESS,result))
            else
                _favoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.EMPTY))
        }catch (throwable:Throwable){
            if(throwable is HttpException && throwable.code() == 401)
                _favoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.UNAUTHORIZED))
            else
                _favoriteBookResponse.postValue(FavouriteBookResponse(RetrofitStatus.FAILURE))
        }
    }
}