package com.example.repository

import com.bookstore.model.request.book.FavouriteBookRequest
import com.bookstore.model.response.book.Book
import com.bookstore.model.response.book.FavouriteBook
import com.example.dao.remote.RemoteBookDAO
import com.example.utils.SessionHelper
import com.example.utils.SessionHelper.asBearer
import okhttp3.ResponseBody
import retrofit2.Response

class BookRespository (
    private val userRepository : UserRepository,
            private val bookDAO : RemoteBookDAO)
{
    suspend fun getBook() : List<Book> = userRepository.checkSession().let {
        if(it != null) return bookDAO.getBook(it.asBearer())
        else  throw SessionHelper.unauthorozation
    }
    suspend fun getFavouriteBook() : FavouriteBook = userRepository.checkSession().let {
        if(it != null) return bookDAO.getFavoriteBook(it.asBearer())
        else  throw SessionHelper.unauthorozation
    }
    suspend fun getBookToBook(favouriteBookRequest: FavouriteBookRequest) : Response<ResponseBody> = userRepository.checkSession().let {
        if(it != null) return bookDAO.addBookToFavourite(it.asBearer(),favouriteBookRequest)
        else  throw SessionHelper.unauthorozation
    }
    suspend fun removeBookFromFavourite(detailId:Int) : Response<ResponseBody> = userRepository.checkSession().let {
        if(it != null) return bookDAO.removeBookFromFavourite(it.asBearer(),detailId)
        else  throw SessionHelper.unauthorozation
    }
}