package com.example.repository

import com.bookstore.model.request.cart.CartRequest
import com.bookstore.model.response.cart.Cart
import com.example.dao.remote.RemoteCartDAO
import com.example.utils.SessionHelper
import com.example.utils.SessionHelper.asBearer
import okhttp3.ResponseBody
import retrofit2.Response

class CartRepository(
    private val userRepository: UserRepository,
    private val cartDAO: RemoteCartDAO
) {
    suspend fun getCart() : Cart = userRepository.checkSession().let {
        if (it != null) return cartDAO.getCart(it.asBearer())
        else throw SessionHelper.unauthorozation
    }
    suspend fun addBookToCart(cartRequest: CartRequest): Response<ResponseBody> =
        userRepository.checkSession().let {
            if (it != null) return cartDAO.addBookToCart(it.asBearer(), cartRequest)
            else throw SessionHelper.unauthorozation
        }
    suspend fun  removeFromCart(detailId:Int) : Response<ResponseBody> =
        userRepository.checkSession().let {
            if (it != null) return cartDAO.removeBookFromCart(it.asBearer(),detailId)
            else throw SessionHelper.unauthorozation
        }
}