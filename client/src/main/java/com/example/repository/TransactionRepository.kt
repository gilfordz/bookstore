package com.example.repository

import android.view.SurfaceControl
import com.bookstore.model.request.transaction.CheckoutRequest
import com.bookstore.model.request.transaction.PaymentRequest
import com.bookstore.model.response.transaction.Transaction
import com.example.dao.remote.RemoteTransactionDAO
import com.example.utils.SessionHelper
import com.example.utils.SessionHelper.asBearer

class TransactionRepository(
    private val userRepository: UserRepository,
    private val transactionDAO: RemoteTransactionDAO
) {
    suspend fun performCheckout(checkoutRequest: CheckoutRequest) : Transaction =
        userRepository.checkSession().let {
            if (it!=null) return transactionDAO.performCheckout(it.asBearer(),checkoutRequest)
            else throw SessionHelper.unauthorozation
        }
    suspend fun performPayment(paymentRequest: PaymentRequest) : Transaction =
        userRepository.checkSession().let {
            if (it !=null) return transactionDAO.performPayment(it.asBearer(),paymentRequest)
            else throw SessionHelper.unauthorozation
        }
}