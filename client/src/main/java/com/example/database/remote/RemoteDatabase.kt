package com.example.database.remote

import com.example.dao.remote.RemoteBookDAO
import com.example.dao.remote.RemoteCartDAO
import com.example.dao.remote.RemoteTransactionDAO
import com.example.dao.remote.RemoteUserDAO
import com.example.utils.Retrofit

object RemoteDatabase {
val userDAO: RemoteUserDAO = Retrofit.getClient().create(RemoteUserDAO::class.java)
val bookDAO: RemoteBookDAO = Retrofit.getClient().create(RemoteBookDAO::class.java)
val cartDAO: RemoteCartDAO = Retrofit.getClient().create(RemoteCartDAO::class.java)
val transactionDAO: RemoteTransactionDAO = Retrofit.getClient().create(RemoteTransactionDAO::class.java)

}