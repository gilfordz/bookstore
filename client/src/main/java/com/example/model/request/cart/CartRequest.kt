package com.bookstore.model.request.cart

import com.example.config.AppConfig

data class CartRequest(
    val bookId: Int,
    val userId: Int = AppConfig.OAUTH_DEFAULT_CUSTOMER_ID
)