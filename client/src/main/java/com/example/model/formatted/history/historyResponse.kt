package com.example.model.formatted.history

import com.bookstore.constant.RetrofitStatus

data class historyResponse(
    val status:RetrofitStatus = RetrofitStatus.UNKNOWN,
    val historyResponse: historyResponse? = null
) {
}