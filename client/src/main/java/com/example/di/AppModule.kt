package com.example.di

import com.example.database.local.LocalDatabase
import com.example.database.remote.RemoteDatabase
import com.example.repository.BookRespository
import com.example.repository.CartRepository
import com.example.repository.TransactionRepository
import com.example.repository.UserRepository
import com.example.signin.SignInViewModel
import com.example.ui.book.DetailBookViewModel
import com.example.ui.checkout.CheckoutViewModel
import com.example.ui.main.MainActivity
import com.example.ui.main.MainViewModel
import com.example.ui.main.fragment.book.BookViewModel
import com.example.ui.main.fragment.cart.CartViewModel
import com.example.ui.search.SearchBookViewModel
import com.example.ui.splashcreen.SplashScreenActivity
import com.example.ui.splashcreen.SplashScreenViewModel
import com.example.ui.wishlist.WishlistViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val AppModule = module {
    // DAO
single { RemoteDatabase.userDAO }
single { RemoteDatabase.bookDAO }
single { RemoteDatabase.cartDAO }
single { RemoteDatabase.transactionDAO }
    single {LocalDatabase.userDAO}
    // REPOSITORY / GUdang
    single { UserRepository(get(),get()) }
    single { BookRespository(get(),get()) }
    single { CartRepository(get(),get()) }
    single { TransactionRepository(get(),get()) }

    //view model
    viewModel { SplashScreenViewModel(get(),get()) }
    viewModel { SignInViewModel(get(),get()) }
    viewModel { MainViewModel(get(),get()) }
    viewModel { BookViewModel(get(),get()) }
    viewModel { CartViewModel(get(),get()) }
    viewModel { DetailBookViewModel(get(),get(),get()) }
    viewModel { SearchBookViewModel(get(),get()) }
    viewModel { WishlistViewModel(get(),get()) }
    viewModel { CheckoutViewModel(get(),get(),get())}

}